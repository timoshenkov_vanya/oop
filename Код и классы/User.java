package OOPtask;

public class User {
    String login;
    private String password;
    Basket userBasket = new Basket();

    User(String login, Basket userBasket, String password) {
        this.userBasket = userBasket;
        setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.length() < 6)
            throw new RuntimeException("Пароль слишком короткий");
        else this.password = password;
    }

}
