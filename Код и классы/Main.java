package OOPtask;

public class Main {
    public static void main(String[] args) {
        Product product1 = new Product("Кепчук", 200, 1.21f);
        Product product2 = new Product("Шаурма: Атомное Трио", 180, 0.5f);
        Product product3 = new Product("Соль: Питерская", 60, 8.33f);
        Product product4 = new Product("Паприка", 30, 9.2f);
        Product product5 = new Product("Майонез: Белый Снег", 123, 5.21f);
        Product product6 = new Product("Бумага Туалетная: Серый Лёд", 20, 0.43f);
        Product[] spices = {product3, product4};
        Product[] sauces = {product1, product5};
        Product[] fastFoods = {product2};
        Product[] housePr = {product6};
        Category spice = new Category("Специи", spices);
        Category sauce = new Category("Соусы", sauces);
        Category fastFood = new Category("Бистро", fastFoods);
        Category house = new Category("Товары для дома", housePr);
        Product[] boughtProducts = {product2, product6, product6, product6, product6};
        Basket myBasket = new Basket(boughtProducts);
        User NAGibATOR_228_1337 = new User("NAGibATOR_228_1337", myBasket, "69_228_1337_4:20");
        System.out.println(NAGibATOR_228_1337);

    }
}
