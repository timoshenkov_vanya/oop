package OOPtask;

public class Product {
    String name;
    int price;
    float rating;

    Product(String name, int price, float rating) {
        this.name = name;
        this.price = price;
        this.rating = rating;
    }
}
